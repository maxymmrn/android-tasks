package com.example.task1.texting;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.example.task1.R;

public class TextingActivity extends FragmentActivity implements InputFragment.TextSender {
    private String value;
    private OutputFragment outputFragment;
    private InputFragment inputFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_texting);

        inputFragment = (InputFragment) getSupportFragmentManager().findFragmentById(R.id.inputFragment);
        outputFragment = (OutputFragment) getSupportFragmentManager().findFragmentById(R.id.outputFragment);

        if (inputFragment == null || outputFragment == null) {
            inputFragment = new InputFragment();
            outputFragment = new OutputFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.inputFragment, inputFragment)
                    .add(R.id.outputFragment, outputFragment)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.inputFragment, inputFragment)
                    .replace(R.id.outputFragment, outputFragment)
                    .commit();
        }


    }

    @Override
    public void onBackPressed() {
        final Intent intent = new Intent();
        intent.putExtra("result", value);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        outputFragment.displayText(value);
        inputFragment.resetValue(value);
    }

    @Override
    public void send(String text) {
        value = text;
        outputFragment.displayText(value);
    }
}
