package com.example.task1.texting;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.task1.R;

public class InputFragment extends Fragment {
    private EditText inputEt;
    private TextSender sender;

    public InputFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        sender = (TextSender) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inputEt = view.findViewById(R.id.inputEt);
        inputEt.addTextChangedListener(getWatcher());
    }

    @Override
    public void onDetach() {
        sender = null;
        super.onDetach();
    }

    public void resetValue(String text) {
        inputEt.setText(text);
    }

    private TextWatcher getWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sender.send(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    public interface TextSender {
        void send(String text);
    }
}